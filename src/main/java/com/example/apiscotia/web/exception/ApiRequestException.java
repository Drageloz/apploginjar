package com.example.apiscotia.web.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ApiRequestException extends RuntimeException{
    public ApiRequestException(String message) {
        super(message);
    }

    public ApiRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiRequestException() {
    }
    
}
