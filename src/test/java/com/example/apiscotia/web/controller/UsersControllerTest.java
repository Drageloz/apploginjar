package com.example.apiscotia.web.controller;

import com.example.apiscotia.domain.dto.Users;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class UsersControllerTest {

    @Autowired
    UsersController usersController;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAll() {
        ResponseEntity response = usersController.getByLastName("suarez");
        assertThat(new ResponseEntity<>(new Users(
                                1,
                                "andres",
                                "suarez",
                                "a@gmail.com",
                                "male",
                                "192.168.1.1"),
                        HttpStatus.OK),
                is(equalTo(response)));
    }

    @Test
    void getByUserName() {
        ResponseEntity response = usersController.getByUserName("andres");
        assertThat(new ResponseEntity<>(
                        new Users(
                                1,
                                "andres",
                                "suarez",
                                "a@gmail.com",
                                "male",
                                "192.168.1.1"),
                        HttpStatus.OK),
                is(equalTo(response)));
    }

    @Test
    void getByLastName() {
        ResponseEntity response = usersController.getByLastName("suarez");
        assertThat(new ResponseEntity<>(
                new Users(
                1,
                "andres",
                "suarez",
                "a@gmail.com",
                "male",
                "192.168.1.1"),
                HttpStatus.OK),
                is(equalTo(response)));
    }
}